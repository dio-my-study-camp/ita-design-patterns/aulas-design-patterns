package com.github.welblade.ita.aulas_design_pattern.gamification.mock;

import com.github.welblade.ita.aulas_design_pattern.gamification.Achievement;
import com.github.welblade.ita.aulas_design_pattern.gamification.AchievementObserver;

public class MockObserver implements AchievementObserver {
    private int calls = 0;
    @Override
    public void achievementUpdate(String user, Achievement a) {
        calls++;
    }

    public int getCalls() {
        return calls;
    }

}
