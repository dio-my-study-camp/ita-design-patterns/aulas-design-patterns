package com.github.welblade.ita.aulas_design_pattern.gamification;

import com.github.welblade.ita.aulas_design_pattern.gamification.mock.MockForumService;

public class MockForumServiceFail extends MockForumService{
    private boolean topicHaveToFail = false;
    private boolean commentHaveToFail = false;
    private boolean likeTopicHaveToFail = false;
    private boolean likeCommentHaveToFail = false;
    
    public boolean isTopicHaveToFail() {
        return topicHaveToFail;
    }

    public void setTopicHaveToFail(boolean topicHaveToFail) {
        this.topicHaveToFail = topicHaveToFail;
    }

    public boolean isCommentHaveToFail() {
        return commentHaveToFail;
    }

    public void setCommentHaveToFail(boolean commentHaveToFail) {
        this.commentHaveToFail = commentHaveToFail;
    }

    public boolean isLikeTopicHaveToFail() {
        return likeTopicHaveToFail;
    }

    public void setLikeTopicHaveToFail(boolean likeTopicHaveToFail) {
        this.likeTopicHaveToFail = likeTopicHaveToFail;
    }

    public boolean isLikeCommentHaveToFail() {
        return likeCommentHaveToFail;
    }

    public void setLikeCommentHaveToFail(boolean likeCommentHaveToFail) {
        this.likeCommentHaveToFail = likeCommentHaveToFail;
    }

    @Override
    public void addTopic(String user, String topic) {
        super.addTopic(user, topic);
        if(isTopicHaveToFail())
            throw new RuntimeException("Exceção forçada.");
    }

    @Override
    public void addComment(String user, String topic, String comment) {
        super.addComment(user, topic, comment);
        if(isCommentHaveToFail())
            throw new RuntimeException("Exceção forçada.");
    }

    @Override
    public void likeComment(String user, String topic, String comment, String commentUser) {
        super.likeComment(user, topic, comment, commentUser);
        if(isLikeCommentHaveToFail())
            throw new RuntimeException("Exceção forçada.");
    }

    @Override
    public void likeTopic(String user, String topic, String topicUser) {
        super.likeTopic(user, topic, topicUser);
        if(isLikeTopicHaveToFail())
            throw new RuntimeException("Exceção forçada.");
    }
}