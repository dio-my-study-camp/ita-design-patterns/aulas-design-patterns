package com.github.welblade.ita.aulas_design_pattern.observer;

public class ContadorPares implements Contador {
    private int qtd;

    @Override
    public int getContagem() {
        return qtd;
    }

    @Override
    public void contar(String palavra) {
        if(palavra.length() % 2 == 0)
            qtd++;
    }

}
