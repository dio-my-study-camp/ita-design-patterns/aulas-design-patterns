package com.github.welblade.ita.aulas_design_pattern.strategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class TarifaEstacionamentoTest {
    @Test
    public void tarifaFixaPorHora() {
        TarifaEstacionamento t = new TarifaEstacionamento(3, new CalculoHora(4));
        double valor = t.valor();
        assertEquals(12.0, valor);
    }

    @Test
    public void tarifaComValorInicialDepoisFixaPorHora() {
        TarifaEstacionamento t = new TarifaEstacionamento(
            5, 
            new CalculoHoraComValorInicial(5, 3, 2)
        );
        double valor = t.valor();
        assertEquals(9.0, valor);
    }
    @Test
    public void tarifaComValorInicialDentroDoLimite() {
        TarifaEstacionamento t = new TarifaEstacionamento(
            3, 
            new CalculoHoraComValorInicial(5, 3, 2)
        );
        double valor = t.valor();
        assertEquals(5.0, valor);
    }
    @Test
    public void tarifaDiaria() {
        TarifaEstacionamento t = new TarifaEstacionamento(
            50, 
            new CalculoDiaria(20.0)
        );
        double valor = t.valor();
        assertEquals(60.0, valor);
    }

    @Test
    public void tarifaDiariaQuantideHorasMenosDeUmDia() {
        TarifaEstacionamento t = new TarifaEstacionamento(
            10, 
            new CalculoDiaria(20.0)
        );
        double valor = t.valor();
        assertEquals(20.0, valor);
    }
}
