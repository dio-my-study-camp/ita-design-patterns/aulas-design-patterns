package com.github.welblade.ita.aulas_design_pattern.gamification;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class PointTest {
    @Test
    public void testObjectCreation(){
        Point point = new Point("Estrelas");

        assertAll(
            () -> assertEquals("Estrelas", point.getName()),
            () -> assertEquals(1, point.getPoints())
        );
    }

    @Test
    public void testAddToAList(){
        Point point = new Point("Estrelas");
        Point point2 = new Point("Estrelas");

        List<Achievement> list = new ArrayList<>();
        point.addTo(list);
        
        assertAll(
            () -> assertEquals(1, list.size()),
            () -> assertEquals("Estrelas", list.get(0).getName())
        );

        point2.addTo(list);
        Point test =  (Point) list.get(0);

        assertAll(
            () -> assertEquals(1, list.size()),
            () -> assertEquals("Estrelas", list.get(0).getName()),
            () -> assertEquals(2, test.getPoints())
        );
    }
    @Test
    public void testAddToAListMultiples(){
        Point point = new Point("Estrelas");

        List<Achievement> list = new ArrayList<>();
        for(var i = 1; i <= 100; i++){
            point = new Point("Estrelas");
            point.addTo(list);
        }

        Point test =  (Point) list.get(0);

        assertAll(
            () -> assertEquals(1, list.size()),
            () -> assertEquals("Estrelas", list.get(0).getName()),
            () -> assertEquals(100, test.getPoints())
        );
    }
}