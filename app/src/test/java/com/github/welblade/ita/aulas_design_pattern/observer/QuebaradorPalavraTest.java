package com.github.welblade.ita.aulas_design_pattern.observer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class QuebaradorPalavraTest {
    @Test
    public void quebrarPalavras(){
        String frase = "O rato roeu a roupa do rei";
        QuebradorPalavra quebrador = new QuebradorPalavra();
        String[] palavras = quebrador.quebrar(frase);
        assertEquals(7, palavras.length);
    }

    @Test
    public void adicionarContadorSimples(){
        String frase = "O rato roeu a roupa do rei";
        QuebradorPalavra quebrador = new QuebradorPalavra();
        quebrador.adicionarContador("SIMPLES", new ContadorSimples());
        quebrador.quebrar(frase);
        assertEquals(7, quebrador.getContador("SIMPLES"));
    }

    @Test
    public void adicionarContadorMaiusculas(){
        String frase = "O Rato roeu a roupa do Rei";
        QuebradorPalavra quebrador = new QuebradorPalavra();
        quebrador.adicionarContador("MAIUSCULA", new ContadorMaiculas());
        quebrador.quebrar(frase);
        assertEquals(3, quebrador.getContador("MAIUSCULA"));
    }

    @Test
    public void adicionarContadorTamanhoPar(){
        String frase = "O Rato roeu a roupa do Rei";
        QuebradorPalavra quebrador = new QuebradorPalavra();
        quebrador.adicionarContador("PARES", new ContadorPares());
        quebrador.quebrar(frase);
        assertEquals(3, quebrador.getContador("PARES"));
    }

    @Test
    public void adicionarContadorIntegrado(){
        String frase = "O Rato roeu a roupa do Rei";
        QuebradorPalavra quebrador = new QuebradorPalavra();
        quebrador.adicionarContador("MAIUSCULA", new ContadorMaiculas());
        quebrador.adicionarContador("SIMPLES", new ContadorSimples());
        quebrador.adicionarContador("PARES", new ContadorPares());
        quebrador.quebrar(frase);
        assertEquals(3, quebrador.getContador("MAIUSCULA"));
        assertEquals(7, quebrador.getContador("SIMPLES"));
        assertEquals(3, quebrador.getContador("PARES"));
    }
    
}
