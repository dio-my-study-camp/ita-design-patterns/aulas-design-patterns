package com.github.welblade.ita.aulas_design_pattern.observer;

import java.util.HashMap;
import java.util.Map;

public class QuebradorPalavra {
    private Map<String, Contador> contadores = new HashMap<String, Contador>();

    public String[] quebrar(String frase) {
        String[] palavras = frase.split(" ");
        for(var palavra :palavras){
            for(var contador :contadores.values()){
                contador.contar(palavra);
            }
        }
        return palavras;
    }

    public void adicionarContador(String nome, Contador contador) {
        this.contadores.put(nome, contador);
    }

    public int getContador(String nome) {
        return this.contadores.get(nome).getContagem();
    }

}
