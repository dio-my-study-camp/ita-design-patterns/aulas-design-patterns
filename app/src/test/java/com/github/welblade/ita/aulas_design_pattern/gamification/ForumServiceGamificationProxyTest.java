package com.github.welblade.ita.aulas_design_pattern.gamification;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import com.github.welblade.ita.aulas_design_pattern.gamification.mock.MockForumService;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class ForumServiceGamificationProxyTest {
    @BeforeAll
    public void init() {
        MemoryAchievementStorage storage = new MemoryAchievementStorage();
        storage.addObserver(new PointObserver("CREATION", "INVENTOR", 100));
        storage.addObserver(new PointObserver("PARTICIPATION", "PART OF THE COMMUNITY", 100));
        AchievementStorageFactory.setAchievementStorage(storage);
        
    }

    @Test
    public void testObjectCreation() {
        ForumService mockService = new MockForumService();
        
        Gamification gamification = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamification);

        assertAll(
            () -> assertTrue(forum.getGamification() instanceof Gamification),
            () -> assertTrue(forum.getForumService() instanceof ForumService)
        );
    }
    @Test
    public void testAddTopicAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
        
        forum.addTopic("Usuario", "Come to Java.");
        Point creation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Usuario", "CREATION");
        Achievement canTalk = AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Usuario", "I CAN TALK");

        assertAll(
            () -> assertEquals(1, mockService.getCalls("addTopic")),
            () -> assertNotNull(creation),
            () -> assertEquals(5, creation.getPoints()),
            () -> assertNotNull(canTalk)
        );
    }
    @Test
    public void testAddTwoTopicsAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
        
        forum.addTopic("Java Islander", "Come to Java.");
        forum.addTopic("Java Islander", "Come to Java please.");
        Point creation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Java Islander", "CREATION");
        List<Achievement> achievements = AchievementStorageFactory
                .getAchievementStorage()
                .getAchievements("Java Islander");
        Achievement[] canTalk = achievements.stream()
            .filter((a) -> a.getName() == "I CAN TALK")
            .toArray(Achievement[]::new);

        assertAll(
            () -> assertEquals(2, mockService.getCalls("addTopic")),
            () -> assertNotNull(creation),
            () -> assertEquals(10, creation.getPoints()),
            () -> assertNotNull(canTalk),
            () -> assertEquals(1, canTalk.length)
        );
    }
    @Test
    public void testAddCommentAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
        
        forum.addComment("Comentador", "Come to Java!", "Come to Kotlin instead.");
        Point participation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Comentador", "PARTICIPATION");
        Achievement letMeAdd = AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Comentador", "LET ME ADD");

        assertAll(
            () -> assertEquals(1, mockService.getCalls("addComment")),
            () -> assertNotNull(participation),
            () -> assertEquals(3, participation.getPoints()),
            () -> assertNotNull(letMeAdd)
        );
    }
    @Test
    public void testLikeTheTopicAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
        
        forum.likeTopic("Usuario", "Come to Java!", "Likable");
        Point creation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Likable", "CREATION");

        assertAll(
            () -> assertEquals(1, mockService.getCalls("likeTopic")),
            () -> assertNotNull(creation),
            () -> assertEquals(1, creation.getPoints())
        );
    }

    @Test
    public void testLikeCommentAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
        
        forum.likeComment(
            "Comentador", 
            "Come to Java!", 
            "Come to Kotlin instead.", 
            "Likable"
        );
        Point participation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Likable", "PARTICIPATION");

        assertAll(
            () -> assertEquals(1, mockService.getCalls("likeComment")),
            () -> assertNotNull(participation),
            () -> assertEquals(1, participation.getPoints())
        );
    }

    @Test
    public void testTodosOsMetodosAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);

        forum.addTopic("Java Tourist", "My thoughts about my travelling to Java.");

        forum.likeTopic(
            "Java Tourist", 
            "My thoughts about my travelling to Java.", 
            "Kotlin Promoter"
        );
        
        forum.addComment(
            "Kotlin Promoter", 
            "My thoughts about my travelling to Java.", 
            "You shoud have come to Kotlin instead."
        );
        forum.addComment(
            "Java Tourist", 
            "My thoughts about my travelling to Java.", 
            "Maybe."
        );

        forum.likeComment(
            "Kotlin Promoter", 
            "My thoughts about my travelling to Java.", 
            "You shoud have come to Kotlin instead.", 
            "Java Tourist"
        );

        forum.likeComment(
            "Java Tourist", 
            "My thoughts about my travelling to Java.", 
            "Maybe.", 
            "Kotlin Promoter"
        );

        Point touristCreation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Java Tourist", "CREATION");
        
        Point touristParticipation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Java Tourist", "PARTICIPATION");
        Achievement touristCanTalk = AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Java Tourist", "I CAN TALK");
        Achievement touristLetMeAdd = AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Kotlin Promoter", "LET ME ADD");

        Point kotlinPromoterParticipation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Kotlin Promoter", "PARTICIPATION");
        Point kotlinPromoterCreation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Kotlin Promoter", "CREATION");
        Achievement kotlinPromoterLetMeAdd = AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("Kotlin Promoter", "LET ME ADD");
        


        assertAll(
            
            () -> assertNotNull(touristCreation),
            () -> assertNotNull(touristParticipation),
            () -> assertNotNull(touristCanTalk),
            () -> assertNotNull(touristLetMeAdd),
            () -> assertEquals(5, touristCreation.getPoints()),
            () -> assertEquals(4, touristParticipation.getPoints()),
            () -> assertNotNull(kotlinPromoterCreation),
            () -> assertNotNull(kotlinPromoterParticipation),
            () -> assertNotNull(kotlinPromoterLetMeAdd),
            () -> assertEquals(1, kotlinPromoterCreation.getPoints()),
            () -> assertEquals(4, kotlinPromoterParticipation.getPoints()),
            () -> assertEquals(1, mockService.getCalls("addTopic")),
            () -> assertEquals(2, mockService.getCalls("addComment")),
            () -> assertEquals(1, mockService.getCalls("likeTopic")),
            () -> assertEquals(2, mockService.getCalls("likeComment"))
        );
    }

    // Atingir 100 pontos de "CREATION" e verificar se o usuário recebe o badge "INVENTOR"
    @Test
    public void testInventorObserverAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
            
        for(var i = 1; i <= 20; i++)
            forum.addTopic("Spammer", "COME TO BALI!!!");
        
        Point spammerCreation = (Point) AchievementStorageFactory
            .getAchievementStorage()
            .getAchievement("Spammer", "CREATION");
        Achievement spammerInventor = AchievementStorageFactory
            .getAchievementStorage()
            .getAchievement("Spammer", "INVENTOR");
        
            assertAll(
                () -> assertNotNull(spammerCreation),
                () -> assertNotNull(spammerInventor, "Não houve dada a badge INVENTOR"),
                () -> assertEquals(100, spammerCreation.getPoints())
            );
        
    }

    @Test
    public void testComunityObserverAndCheckAchievement() {
        MockForumService mockService = new MockForumService();
        
        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
            
        for(var i = 1; i <= 34; i++)
            forum.addComment("Spammer", "COME TO BALI!!!", "COME TO BALI!!!");
        
        Point spammerParticipation = (Point) AchievementStorageFactory
            .getAchievementStorage()
            .getAchievement("Spammer", "PARTICIPATION");
        Achievement spammerCommunity = AchievementStorageFactory
            .getAchievementStorage()
            .getAchievement("Spammer", "PART OF THE COMMUNITY");
        
            assertAll(
                () -> assertNotNull(spammerParticipation),
                () -> assertNotNull(spammerCommunity, "Não houve dada a badge INVENTOR"),
                () -> assertEquals(102, spammerParticipation.getPoints())
            );
        
    }
    @Test
    public void testTryAddTopicFailAndCheckAchievement() {
        MockForumServiceFail mockService = new MockForumServiceFail();
        mockService.setTopicHaveToFail(true);

        Gamification gamefication = new Gamification(
            AchievementStorageFactory.getAchievementStorage()
        );
        ForumServiceGamificationProxy forum = 
            new ForumServiceGamificationProxy(mockService, gamefication);
        
        assertThrows(
            RuntimeException.class, 
            () -> forum.addTopic("UsuarioErrado", "Come to Java.")
        );
        Point creation = (Point) AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("UsuarioErrado", "CREATION");
        Achievement canTalk = AchievementStorageFactory
                .getAchievementStorage()
                .getAchievement("UsuarioErrado", "I CAN TALK");

        assertAll(
            () -> assertEquals(1, mockService.getCalls("addTopic")),
            () -> assertNull(creation),
            () -> assertNull(canTalk)
        );
    }
}