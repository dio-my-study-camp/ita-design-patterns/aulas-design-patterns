package com.github.welblade.ita.aulas_design_pattern.gamification.mock;

import java.util.HashMap;
import java.util.Map;

import com.github.welblade.ita.aulas_design_pattern.gamification.ForumService;

public class MockForumService implements ForumService {
    Map<String, Integer> calls = new HashMap<>();
    
    @Override
    public void addTopic(String user, String topic) {
        addCall("addTopic");
    }

    @Override
    public void addComment(String user, String topic, String comment) {
        addCall("addComment");
    }

    @Override
    public void likeTopic(String user, String topic, String topicUser) {
        addCall("likeTopic");
    }

    @Override
    public void likeComment(String user, String topic, String comment, String commentUser) {
        addCall("likeComment");
    }

    private void addCall(String method){
        if(calls.containsKey(method)){
            calls.replace(method, calls.get(method) + 1);        
        }else{
            calls.put(method, 1);
        }
    }

    public int getCalls(String method){
        try {
            return calls.get(method);
        } catch (java.lang.NullPointerException e) {
            return 0;
        }   
    }

}
