package com.github.welblade.ita.aulas_design_pattern.gamification.mock;

import com.github.welblade.ita.aulas_design_pattern.gamification.Point;

public class MockPointAchievement extends Point {

    public MockPointAchievement(String name, int value) {
        super(name);
        q = value;
    }
}
