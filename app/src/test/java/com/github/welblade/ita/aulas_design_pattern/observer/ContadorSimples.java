package com.github.welblade.ita.aulas_design_pattern.observer;

public class ContadorSimples implements Contador {
    private int qtd = 0;
    @Override
    public int getContagem() {
        return qtd;
    }
    @Override
    public void contar(String palavra) {
        qtd += 1;        
    }

}
