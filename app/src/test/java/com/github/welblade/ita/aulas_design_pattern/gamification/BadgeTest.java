package com.github.welblade.ita.aulas_design_pattern.gamification;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class BadgeTest {
    @Test
    public void testObjectCreation(){
        Badge badge = new Badge("I can talk!");

        assertEquals("I can talk!", badge.getName());
    }

    @Test
    public void testAddToAList(){
        Badge badge = new Badge("I can talk!");
        Badge badge2 = new Badge("I can talk!");

        List<Achievement> list = new ArrayList<>();
        badge.addTo(list);
        
        assertAll(
            () -> assertEquals(1, list.size()),
            () -> assertEquals("I can talk!", list.get(0).getName())
        );

        badge2.addTo(list);

        assertAll(
            () -> assertEquals(1, list.size()),
            () -> assertEquals("I can talk!", list.get(0).getName())
        );
    }
}