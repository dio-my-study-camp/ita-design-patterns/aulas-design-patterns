package com.github.welblade.ita.aulas_design_pattern.gamification;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.github.welblade.ita.aulas_design_pattern.gamification.mock.MockPointAchievement;
import com.github.welblade.ita.aulas_design_pattern.gamification.mock.MockStorage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class InventorObserverTest {
    private MockStorage mock;

    @BeforeEach
    public void init(){
        mock = new MockStorage();
        AchievementStorageFactory.setAchievementStorage(mock);
    }

    @Test
    public void testAddObserver() {
        AchievementObserver observer = new PointObserver("CREATION","INVENTOR", 100);
        
        mock.addObserver(observer);
        var point = new MockPointAchievement("CREATION", 100);
        mock.callObserver("user", point);
        assertEquals(1, mock.getCalls("addAchievement"));
    }

    @Test
    public void testBadgeAchievement() {
        AchievementObserver observer = new PointObserver("CREATION","INVENTOR", 100);

        var point = new MockPointAchievement("SUPERHEROE", 100);

        mock.addObserver(observer);
        mock.callObserver("user", point);
        assertEquals(0, mock.getCalls("addAchievement"));
    }

    @Test
    public void testAnotherAchievement() {
        AchievementObserver observer = new PointObserver("CREATION","INVENTOR", 100);

        var badge = new Badge("CREATION");
        mock.addObserver(observer);
        mock.callObserver("user", badge);
        assertEquals(0, mock.getCalls("addAchievement"));
    }
}