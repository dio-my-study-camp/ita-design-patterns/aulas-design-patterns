package com.github.welblade.ita.aulas_design_pattern.builder;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class GeradorNomeTest {
    @Test
    public void testRetornaNomeBase(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarPessoa()
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Wellington", nome);
    }

    @Test
    public void testRetornaMestre(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarMestre()
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Mestre Wellington", nome);
    }

    @Test
    public void testRetornaDoutor(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarDoutor()
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Doutor Wellington", nome);
    }

    @Test
    public void testRetornaExcelentissiomoDoutor(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarDoutor()
            .excelentissimo()
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Excelentíssimo Doutor Wellington", nome);
    }

    @Test
    public void testRetornaMaginificoMestre(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarMestre()
            .magnifico()
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Magnífico Mestre Wellington", nome);
    }

    @Test
    public void testRetornaExcelentissimoMaginificoMestre(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarMestre()
            .excelentissimo()
            .magnifico()
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Excelentíssimo Magnífico Mestre Wellington", nome);
    }

    @Test
    public void testRetornaPessoaDeAlgumLugar(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarPessoa()
            .de("Cubatão")
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Wellington de Cubatão", nome);
    }

    @Test
    public void testRetornaMaginificoDoutorDeAlgumLugar(){
        GeradorNome gn = new GeradorNomeBuilder()
            .criarDoutor()
            .de("Cubatão")
            .magnifico()
            .de("São Paulo")
            .gerar();
        
        String nome = gn.gerarNome("Wellington");
        assertEquals("Magnífico Doutor Wellington de Cubatão de São Paulo", nome);
    }
}