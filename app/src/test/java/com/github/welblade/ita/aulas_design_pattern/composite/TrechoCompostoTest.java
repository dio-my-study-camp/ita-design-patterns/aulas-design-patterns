package com.github.welblade.ita.aulas_design_pattern.composite;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class TrechoCompostoTest {
    @Test
    public void testeTrechoAereoComEscala() {
        TrechoAereoSimples t1 = new TrechoAereoSimples("GRU", "FOR", 200, 400);
        TrechoAereoSimples t2 = new TrechoAereoSimples("FOR", "NAT", 150, 200);
        TrechoAereo t3 = new TrechoAereoComEscala(t1, t2);

        assertAll(
            () -> assertEquals("GRU", t3.getOrigem()),
            () -> assertEquals("NAT", t3.getDestino()),
            () -> assertEquals(350, t3.getCusto()),
            () -> assertEquals(600, t3.getDistancia())
        );
        
    }

    @Test
    public void testeTrechoAereoCompEscalaAeroportoDiferente() {
        TrechoAereoSimples t1 = new TrechoAereoSimples("GRU", "BSD", 200, 400);
        TrechoAereoSimples t2 = new TrechoAereoSimples("FOR", "NAT", 150, 200);
        assertThrows(RuntimeException.class, () -> new TrechoAereoComEscala(t1, t2)) ;
    }

    @Test
    public void testeTrechoAereoComConexao() {
        TrechoAereoSimples t1 = new TrechoAereoSimples("GRU", "FOR", 200, 400);
        TrechoAereoSimples t2 = new TrechoAereoSimples("FOR", "NAT", 150, 200);
        TrechoAereo t3 = new TrechoAereoComConexao(t1, t2, 80);

        assertAll(
            () -> assertEquals("GRU", t3.getOrigem()),
            () -> assertEquals("NAT", t3.getDestino()),
            () -> assertEquals(430, t3.getCusto()),
            () -> assertEquals(600, t3.getDistancia())
        );
    }

    @Test
    public void testeTrechoAereoComEscalaEConexao() {
        TrechoAereoSimples t1 = new TrechoAereoSimples("GRU", "FOR", 200, 400);
        TrechoAereoSimples t2 = new TrechoAereoSimples("FOR", "NAT", 150, 200);
        TrechoAereoSimples t3 = new TrechoAereoSimples("NAT", "BSB", 200, 400);
        TrechoAereo escala = new TrechoAereoComEscala(t1, t2);
        TrechoAereo conexao = new TrechoAereoComConexao(escala, t3, 80);

        assertAll(
            () -> assertEquals("GRU", conexao.getOrigem()),
            () -> assertEquals("BSB", conexao.getDestino()),
            () -> assertEquals(630, conexao.getCusto()),
            () -> assertEquals(1000, conexao.getDistancia())
        );
        
    }
}
