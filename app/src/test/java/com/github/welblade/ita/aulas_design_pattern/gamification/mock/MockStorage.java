package com.github.welblade.ita.aulas_design_pattern.gamification.mock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.welblade.ita.aulas_design_pattern.gamification.Achievement;
import com.github.welblade.ita.aulas_design_pattern.gamification.AchievementObserver;
import com.github.welblade.ita.aulas_design_pattern.gamification.AchievementStorage;

public class MockStorage implements AchievementStorage {
    Map<String, Integer> calls = new HashMap<>();
    AchievementObserver observer;
    Achievement achievement;

    @Override
    public void addAchievement(String user, Achievement a) {  
        addCall("addAchievement");
    }

    @Override
    public List<Achievement> getAchievements(String user) {
        addCall("getAchievements");
        return null;
    }

    @Override
    public Achievement getAchievement(String user, String achievementName) {
        addCall("getAchievement");
        return achievement; 
    }

    private void addCall(String method){
        if(calls.containsKey(method)){
            calls.replace(method, calls.get(method) + 1);        
        }else{
            calls.put(method, 1);
        }
        
    }

    public int getCalls(String method){
        try {
            return calls.get(method);
        } catch (java.lang.NullPointerException e) {
            return 0;
        }   
    }
    
    @Override
    public void addObserver(AchievementObserver observer) {
        this.observer = observer; 
    }

    public void callObserver(String user, Achievement achievement){
        this.achievement = achievement;
        observer.achievementUpdate(user, this.achievement);
    }

}
