package com.github.welblade.ita.aulas_design_pattern.observer;

public class ContadorMaiculas implements Contador {
    private int qtd;

    @Override
    public int getContagem() {
        return qtd;
    }

    @Override
    public void contar(String palavra) {
        if(Character.isUpperCase(palavra.charAt(0)))
            qtd++;        
    }

}
