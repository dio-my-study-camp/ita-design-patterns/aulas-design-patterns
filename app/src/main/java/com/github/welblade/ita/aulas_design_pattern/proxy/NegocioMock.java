package com.github.welblade.ita.aulas_design_pattern.proxy;


public class NegocioMock implements InterfaceNegocio{
    private boolean acessado;

    public boolean foiAcessado() {
        return acessado;
    }

    @Override
    public void executaTransacao() {
        acessado = true;
    }

    @Override
    public void cancelaTransacao() {
        acessado = true;
    }

}
