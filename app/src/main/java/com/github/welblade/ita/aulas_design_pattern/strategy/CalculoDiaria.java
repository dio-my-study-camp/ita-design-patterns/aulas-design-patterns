package com.github.welblade.ita.aulas_design_pattern.strategy;

public class CalculoDiaria implements Calculo {
    private double valorDiaria;

    public CalculoDiaria(double valorDiaria) {
        this.valorDiaria = valorDiaria;
    }

    @Override
    public double calcularTarifa(int qtdHoras) {
        return valorDiaria * Math.ceil(qtdHoras / 24.0);
    }

}
