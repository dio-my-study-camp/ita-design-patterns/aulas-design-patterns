package com.github.welblade.ita.aulas_design_pattern.proxy;

public class SegurançaNegocio implements InterfaceNegocio {
    InterfaceNegocio subordinado;
    Usuario usuario;

    public SegurançaNegocio(InterfaceNegocio negocio, Usuario usuario) {
        subordinado = negocio;
        this.usuario = usuario;
    }

    @Override
    public void executaTransacao() {
        if(!usuario.verificaAcesso("InterfaceNegocio", "executaTransacao"))
            throw new RuntimeException();
        subordinado.executaTransacao();
    }

    @Override
    public void cancelaTransacao() {
        if(!usuario.verificaAcesso("InterfaceNegocio", "cancelaTransacao"))
            throw new RuntimeException();
        subordinado.cancelaTransacao(); 
    }

}
