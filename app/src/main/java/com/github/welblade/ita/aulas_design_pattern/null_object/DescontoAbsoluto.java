package com.github.welblade.ita.aulas_design_pattern.null_object;

public class DescontoAbsoluto implements Desconto {

    public DescontoAbsoluto(double valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    private double valorDesconto;

    @Override
    public double darDesconto(double valorOriginal) {
        return valorOriginal - valorDesconto;
    }

}
