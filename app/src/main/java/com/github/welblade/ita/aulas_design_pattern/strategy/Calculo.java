package com.github.welblade.ita.aulas_design_pattern.strategy;

public interface Calculo {

    double calcularTarifa(int qtdHoras);

}
