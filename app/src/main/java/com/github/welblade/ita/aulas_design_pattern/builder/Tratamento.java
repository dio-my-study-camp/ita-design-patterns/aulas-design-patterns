package com.github.welblade.ita.aulas_design_pattern.builder;

public interface Tratamento {
    String retornarPronome();
}
