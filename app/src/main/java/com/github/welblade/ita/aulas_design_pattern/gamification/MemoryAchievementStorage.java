package com.github.welblade.ita.aulas_design_pattern.gamification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemoryAchievementStorage implements AchievementStorage {
    public MemoryAchievementStorage(){
        this.achievements = new HashMap<>();
        observers = new ArrayList<>();
    }

    Map<String, List<Achievement>> achievements;
    List<AchievementObserver> observers;

    @Override
    public void addAchievement(String user, Achievement a) {
        if(getAchievements(user) == null){
            achievements.put(user, new ArrayList<>());
        }
        a.addTo(getAchievements(user));
        notifyObservers(user, a);
    }

    @Override
    public List<Achievement> getAchievements(String user) {
        return achievements.get(user);
    }

    @Override
    public Achievement getAchievement(String user, String achievementName) {
        List<Achievement> list = getAchievements(user);
        if(list == null){
            return null;
        }
        
        return list
            .stream()
            .filter((achievement) -> achievement.getName() == achievementName)
            .findFirst()
            .orElse(null);
    }

    @Override
    public void addObserver(AchievementObserver observer) {
        observers.add(observer);
    }

    public void notifyObservers(String user, Achievement a){
        for(var observer : observers){
            observer.achievementUpdate(user, a);
        }
    }
}