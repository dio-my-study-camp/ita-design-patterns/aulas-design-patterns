package com.github.welblade.ita.aulas_design_pattern.gamification;

public class PointObserver implements AchievementObserver {

    public PointObserver(String pointName, String badgeName, int pointsNeeded){
        this.pointName = pointName;
        this.badgeName = badgeName;
        this.pointsNeeded = pointsNeeded;
    }
    private String pointName;
    private String badgeName;
    private int pointsNeeded;
 
    @Override
    public void achievementUpdate(String user, Achievement a) {
        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();
        if (a.getName() == pointName && a instanceof Point) {
            Point points = (Point) storage.getAchievement(user, a.getName());
            if(points.getPoints() == pointsNeeded){
                storage.addAchievement(user, new Badge(badgeName));
            }
        }
    }
}