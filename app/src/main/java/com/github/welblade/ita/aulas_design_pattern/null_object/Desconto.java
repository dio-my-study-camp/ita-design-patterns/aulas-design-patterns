package com.github.welblade.ita.aulas_design_pattern.null_object;

public interface Desconto {
    public double darDesconto(double valorOriginal);
}
