package com.github.welblade.ita.aulas_design_pattern.builder;

public class GeradorNome {
    Tratamento tratamento = new TratamentoNull();

    public String gerarNome(String nomeBase){
        return tratamento.retornarPronome() + getTratamento() + nomeBase;
    }

    protected String getTratamento() {
        return "";
    }

    public void setTratamentoStrategy(Tratamento tratamento) {
        this.tratamento = tratamento;
    }

    public Tratamento getTratamentoStrategy() {
        return tratamento;
    }
}
