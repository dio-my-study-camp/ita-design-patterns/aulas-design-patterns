package com.github.welblade.ita.aulas_design_pattern.gamification;

import java.util.List;
import java.util.NoSuchElementException;

public class Point extends Achievement{
    protected int q = 0;

    public Point(String name) {
        super(name);
        this.q = 1;
    }

    public int getPoints(){
        return q;
    }

    public void addPoints(){
        q++;
    }

    @Override
    public void addTo(List<Achievement> achievements) {
        try {
            Point points = (Point) achievements.stream()
                .filter((point) -> point.getName() == this.getName())
                .findFirst()
                .orElseThrow();
            points.addPoints(); 
        } catch (NoSuchElementException err) {
            achievements.add(this);
        }
    }
}