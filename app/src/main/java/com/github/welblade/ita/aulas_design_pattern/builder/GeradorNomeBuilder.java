package com.github.welblade.ita.aulas_design_pattern.builder;

public class GeradorNomeBuilder {
    GeradorNome gn;


    public GeradorNomeBuilder criarPessoa() {
        gn = new GeradorNome();
        return this;
    }

    public GeradorNome gerar() {
        return gn;
    }

    public GeradorNomeBuilder criarMestre() {
        gn = new GeradorMestre();
        return this;
    }

    public GeradorNomeBuilder criarDoutor() {
        gn = new GeradorDoutor();
        return this;
    }

    public GeradorNomeBuilder excelentissimo() {
        setTratamentoStrategy(new TratamentoExcelentissimo());
        return this;
    }

    public GeradorNomeBuilder magnifico() {
        setTratamentoStrategy(new TratamentoMagnifico());
        return this;
    }

    private void setTratamentoStrategy(Tratamento tratamento){
        if (gn.getTratamentoStrategy() instanceof TratamentoNull){
            gn.setTratamentoStrategy(tratamento);
        }else{
            gn.setTratamentoStrategy(new TratamentoComposite(gn.getTratamentoStrategy(), tratamento));
        }
    }

    public GeradorNomeBuilder de(String local) {
        gn = new GeradorNomeProxy(gn, local);
        return this;
    }

}
