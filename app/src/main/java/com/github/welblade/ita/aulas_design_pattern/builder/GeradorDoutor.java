package com.github.welblade.ita.aulas_design_pattern.builder;

public class GeradorDoutor extends GeradorNome {

    @Override
    protected String getTratamento() {
        return "Doutor ";
    }
    
}
