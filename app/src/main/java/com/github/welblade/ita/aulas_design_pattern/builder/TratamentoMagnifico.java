package com.github.welblade.ita.aulas_design_pattern.builder;

public class TratamentoMagnifico implements Tratamento {

    @Override
    public String retornarPronome() {
        return "Magnífico ";
    }

}
