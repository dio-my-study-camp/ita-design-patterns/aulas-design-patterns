package com.github.welblade.ita.aulas_design_pattern.null_object;

public class Item {

    
    public Item(String nome, double valor) {
        super();
        this.nome = nome;
        this.valor = valor;
    }

    public Item(String nome, double valor, Desconto desconto) {
        super();
        this.nome = nome;
        this.valor = valor;
        this.desconto = desconto;
    }

    private String nome;
    private double valor;
    private Desconto desconto = new SemDesconto();

    public String toString(){
        return nome + " R$" + desconto.darDesconto(valor);
    }

    public double precoQuantidade(int qtd) {
        return desconto.darDesconto(valor) * qtd;
    }

}
