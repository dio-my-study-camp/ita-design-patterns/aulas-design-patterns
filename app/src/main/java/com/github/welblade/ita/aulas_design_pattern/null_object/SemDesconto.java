package com.github.welblade.ita.aulas_design_pattern.null_object;

public class SemDesconto implements Desconto{

    @Override
    public double darDesconto(double valorOriginal) {
        return valorOriginal;
    }
    
}
