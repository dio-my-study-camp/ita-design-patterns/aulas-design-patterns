package com.github.welblade.ita.aulas_design_pattern.strategy;

public class CalculoHora implements Calculo {
    private double valorHora;
    public CalculoHora(double valorHora) {
        this.valorHora = valorHora;
    }

    @Override
    public double calcularTarifa(int qtdHoras) {
        return qtdHoras * valorHora;
    }

}
