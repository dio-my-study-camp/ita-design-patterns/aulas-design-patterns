package com.github.welblade.ita.aulas_design_pattern.builder;

public class TratamentoExcelentissimo implements Tratamento{

    @Override
    public String retornarPronome() {
        return "Excelentíssimo ";
    }

}
