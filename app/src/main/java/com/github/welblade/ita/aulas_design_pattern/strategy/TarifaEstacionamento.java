package com.github.welblade.ita.aulas_design_pattern.strategy;

public class TarifaEstacionamento {
    private int qtdHoras;
    private Calculo calculo;

    public TarifaEstacionamento(int qtdHoras, Calculo calculo) {
        this.qtdHoras = qtdHoras;
        this.calculo = calculo;
    }

    public double valor() {
        return calculo.calcularTarifa(qtdHoras);
    }

}
