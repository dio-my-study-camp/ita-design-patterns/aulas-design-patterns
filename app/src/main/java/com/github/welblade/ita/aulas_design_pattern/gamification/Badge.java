package com.github.welblade.ita.aulas_design_pattern.gamification;

import java.util.List;

public class Badge extends Achievement {

    public Badge(String name) {
        super(name);
    }

    @Override
    public void addTo(List<Achievement> achievements) {
       if(!achievements.stream().anyMatch(
           (badge)->badge.getName() == this.getName())
        ){
            achievements.add(this);
       }
        
    }

}
