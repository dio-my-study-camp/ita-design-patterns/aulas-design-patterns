# Desenvolvimento Ágil com Padrões de Projeto
por Instituto Tecnológico da Aeronáutica
### Aulas Desenvolvimento Ágil com Padrões de Projeto
#### Hands on 

Semana 1 - Revisão de conceitos de orientação a objetos pertinentes a padrões de projeto; introdução a padrões de projeto, apresentação do padrão Strategy e do Static Factory Methods.
- Padrão Strategy
- Static Factory Methods

Semana 2 - Padrões utilizando herança e/ou composição.
- Padrão Null Object
- Hook Methods
- Padrão Template Method
- Parte 2: Padrão Factory Method
- Padrão Observer

Semana 3 - Composição recursiva com os padrões Composite e Chain of Responsibility, encapsulamento com padrões Proxy, Decorator e Adapter, e padrão Singleton para limitar a instancia de uma classe a apenas um único objeto
- Padrão Composite
- Padrão Proxy

Semana 4 - Criação de objetos com padrões Abstract Factory e Builder, modularização com padrão de projeto Dependency Injection, 
- Padrão Builder
- Componente de Gamificação
